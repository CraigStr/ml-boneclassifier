# **CS401 Assignment 2**

Repository URL: https://gitlab.com/CraigStr/ml-boneclassifier

This assignment was written using **Google Colaboratory**.

Assignment URI: https://colab.research.google.com/drive/19WR7Vb0Gi13WiW5sCAviZdGvlL_Lb-pe?usp=sharing

If any errors occur, do not hesitate to contact me.

# Repo File Structure
```
README.md
.gitignore
resources/
L...bone_model/      # Pre-trained model. Copy this folder (not just contents) into Colaboratory
L...out/             # Contains test-out.txt
L...saved_files/     # Plotted graphs
```

# Colaboratory File Structure

All files that are of any consequence are in the `/content/` directory

```
/content/
L...sample_data/        # Place test-in.txt and train-io.txt here
L.......test-in.txt
L.......train-io.txt
L...out/                # test-out.txt will be generated here
L...saved_files/        # Saved plots and graphs will be generated here
L...bone_model/         # If training model from scratch, the trained
                          model will be automatically placed here.
                        # If not training from scratch, place the
                          `bone_model` folder here yourself
```

# Inputs

No manupulation is needed prior to uploading `test-in.txt` and `train-io.txt`. The manipulation is all done in the script

# Usage

## Upload training and test data

Upload `test-in.txt` and `train-io.txt` into `/content/sample_data/` as shown in

```
/content/
L...sample_data/
L.......test-in.txt
L.......train-io.txt
```

---

## Use pre-trained model

If you want to use the model I have given with this link

1. upload the **bone_model** to `/content`
   i.e.
   ```
   /content/
   L...bone_model
   ```
2. Set `MODEL_ALREADY_SAVED` to `True` in the below cell

---

## Running Code

Supplied link is simply a **Jupyter Notebook**.

After deciding whether to train model from scratch,  
Run from the menu

```
Runtime > Run All
```

Google Colaboratory allows for interactive inputs, which I have included at certain parts of the code. These are preset to the inputs I used when generating the text-out file I provided.  
The only one you probably will need to change is **the checkbox to say whether or not you have provided a pre-trained model** (3rd cell in notebook)

# Process Description

I chose to run a Binary Classifier on this dataset

1. **Import** relevant packages
2. Create the correct **file structure in Google Colab** for my outputted files
3. **Read** `train-io.txt` into a **pandas dataframe**
4. **Describe train-io data**, to see **mean, std and percentiles** for each feature
5. **Split** train-io data into **training and validation sets**. 70/30 split
6. **Create model**,
   ```python
   tf.keras.layers.Flatten(input_shape=(12,)),
   tf.keras.layers.Dense(144, activation=tf.nn.relu),
   tf.keras.layers.Dense(144, activation=tf.nn.relu),
   tf.keras.layers.Dense(1, activation=tf.nn.sigmoid),
   ```
7. **Compiled Model**
   - Optimizer: **Adam**
   - Loss Function: **Binary Cross Entropy**
8. 1. If pre-trained model supplied
      1. **Load model**
   2. Else
      1. **Train model** on the Training Set
         - 25 epochs
         - 25 batch size
      2. **Save model to file** for future
      3. **Plot accuracy and loss over time**
9. **Evaluate model** on **Validation Set**
10. **Read** `test-in.txt`
11. **Predicted probabilities** for Test Set
12. **Rounded probabilites** to nearest integer
13. **Plotted ROC Curve** and **Confusion Matrix**
14. **Saved** `test-out.txt` to **file**
15. Compressed Model, plot images and test-out.txt to allow for download within Colab

# Plots

## ROC Curve

![ROC Curve](resources/saved_files/roc_curve.png)

## Confusion Matrix

![ROC Curve](resources/saved_files/confusion_matrix.png)

## Accuracy & Loss vs Time

![ROC Curve](resources/saved_files/accuracy_graph.png)
